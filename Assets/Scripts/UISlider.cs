﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISlider
{
    public string name;
    public Vector3 minDefaultMax;

    public UISlider(string newName, float newMin, float newDefault, float newMax)
    {
        name = newName;
        minDefaultMax = new Vector3(newMin, newDefault, newMax);
    }
}
