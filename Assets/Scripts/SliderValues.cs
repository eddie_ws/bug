﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class SliderValues : MonoBehaviour
{
    public TextMeshProUGUI name;
    public Slider slider;
    
    public void SetSliderValue(float min, float defaultValue, float max,  string sliderName)
    {
        name.text = sliderName;
        slider.minValue = min;
        slider.maxValue = max;
        slider.value = defaultValue;
    }

    public float GetSliderValue()
    {
        return slider.value;
    }
}
