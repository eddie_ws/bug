﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public Vector3 minPlatformPosition;
    public Vector3 maxPlatformPosition;

    public float minSpeed;
    public float maxSpeed;
    public float defaultSpeed;

    Rigidbody2D playerRB;

    Vector3 newPosition;

    float lerper;
    bool moveToMinPos;
    bool moveToMaxPos = true;
    Vector3 direction;

    UISlider[] speeds;

    private void Awake()
    { 

        transform.position = minPlatformPosition;

        direction = (maxPlatformPosition - minPlatformPosition).normalized;

    }

    private void Update()
    {
        newPosition = Vector3.zero;

        if (moveToMaxPos)
        {
            newPosition = Vector3.Lerp(minPlatformPosition, maxPlatformPosition, lerper);

            lerper += defaultSpeed * Time.deltaTime;

            if(lerper >= 1f)
            {
                moveToMaxPos = false;
                moveToMinPos = true;
            }
        }

        if (moveToMinPos)
        {
            newPosition = Vector3.Lerp(minPlatformPosition, maxPlatformPosition, lerper);

            lerper -= defaultSpeed * Time.deltaTime;

            if (lerper <= 0f)
            {
                moveToMaxPos = true;
                moveToMinPos = false;
            }
        }

        transform.position = newPosition;        
    }




    //When the object is selected by the player, we send the UI the Information it needs (The UISlider list we created in Awake() )
    void recieveUIRequest()
    {
        UISlider speed1 = new UISlider("PLATFORM SPEED", minSpeed, defaultSpeed, maxSpeed);

        speeds = new UISlider[1];

        speeds[0] = speed1; 

        ObjectReciever.Instance.recieveObjectValues(speeds);
    }


    //If the player changes the slider values, we get the updated slider values in a float list. The order of the float list is the same as the UISlider list we created before
    void recieveUpdatedValuesFromUI(float[] newValues)
    {
        defaultSpeed = newValues[0];
    }

    
}
