﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectReciever : MonoBehaviour
{
    public static ObjectReciever Instance { get; private set; }
    SelectObject selectObject;
    public GameObject uiSliderPrefab;
    public RectTransform sliderList;

    bool sliderSet;

    List<SliderValues> currentActiveSliders = new List<SliderValues>();

    void Awake()
    {
        // Save a reference to the ObjectReciever component as our singleton instance
        Instance = this;
        selectObject = GetComponent<SelectObject>();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(selectObject.selectedObject != null && sliderSet)
        {
            float[] updatedValues = new float [currentActiveSliders.Count];

            for (int i = 0; i < updatedValues.Length; i++)
            {
                updatedValues[i] = currentActiveSliders[i].GetSliderValue();
            }

            selectObject.selectedObject.SendMessage("recieveUpdatedValuesFromUI", updatedValues, SendMessageOptions.DontRequireReceiver);
        }
    }

    public void recieveObjectValues(UISlider[] uISliders)
    {
        foreach (UISlider slider in uISliders)
        {
            GameObject newSlider = Instantiate(uiSliderPrefab, sliderList);
            SliderValues newSliverValues = newSlider.GetComponent<SliderValues>();
            newSliverValues.SetSliderValue(slider.minDefaultMax.x, slider.minDefaultMax.y, slider.minDefaultMax.z, slider.name);
            currentActiveSliders.Add(newSlider.GetComponent<SliderValues>());
            sliderSet = true;
        }
    }

    public void clearObjectValues()
    {
        foreach (SliderValues slider in currentActiveSliders)
        {
            Destroy(slider.gameObject);
        }

        currentActiveSliders.Clear();

        sliderSet = false;
    }



}
