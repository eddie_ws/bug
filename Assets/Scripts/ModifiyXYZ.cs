﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModifiyXYZ : MonoBehaviour
{
    public Vector2 minXY;
    public Vector2 defaultXY;
    public Vector2 maxXY;

    public bool useRotation;
    public bool useVisibility = false;

    public float minRotation;
    public float defaultRotation;
    public float maxRotation;

    public float defaultVisibility;

    UISlider[] positions;

    private void Awake()
    {
        transform.position = defaultXY;

        if (useRotation)
        {
            transform.localEulerAngles = new Vector3(0, 0, defaultRotation);
        }
    }


    //When the object is selected by the player, we send the UI the Information it needs (The UISlider list we created in Awake() )
    void recieveUIRequest()
    {
        UISlider positionX = new UISlider("POSITION X", minXY.x, defaultXY.x, maxXY.x);
        UISlider positionY = new UISlider("POSITION Y", minXY.y, defaultXY.y, maxXY.y);

        positions = new UISlider[2];

        if (useRotation)
        {
            positions = new UISlider[3];

            if (useVisibility)
            {
                positions = new UISlider[4];
                UISlider visibility = new UISlider("VISIBILITY", 0, defaultVisibility, 1);
                positions[3] = visibility;
            }

            UISlider rotationZ = new UISlider("ROTATION", minRotation, defaultRotation, maxRotation);
            positions[2] = rotationZ;
        }

        positions[0] = positionX;
        positions[1] = positionY;
        
        ObjectReciever.Instance.recieveObjectValues(positions);
    }


    //If the player changes the slider values, we get the updated slider values in a float list. The order of the float list is the same as the UISlider list we created before
    void recieveUpdatedValuesFromUI(float[] newValues)
    {
        defaultXY = new Vector2(newValues[0], newValues[1]);
        transform.position = defaultXY;

        if (useRotation)
        {
            defaultRotation = newValues[2];
            transform.localEulerAngles = new Vector3(0, 0, defaultRotation);
        }

        if (useVisibility)
        {
            defaultVisibility = newValues[3];
            SpriteRenderer[] allSprites = GetComponentsInChildren<SpriteRenderer>();
            foreach (SpriteRenderer sprite in allSprites)
            {
                sprite.color = new Color(1, 1, 1, defaultVisibility);
            }

            if(defaultVisibility <= 0.15)
            {
                GetComponent<Collider2D>().enabled = false;
            }

            else
            {
                GetComponent<Collider2D>().enabled = true;
            }
        }
    }
}
