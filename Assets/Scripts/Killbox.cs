﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Killbox : MonoBehaviour
{

    EntityAttributes entity;


    private void Awake()
    {
        entity = GetComponent<EntityAttributes>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            IDamageable entityInterface = (IDamageable)collision.GetComponent(typeof(IDamageable));
            if (entityInterface != null)
            {
                entityInterface.GetDamage(entity.dealDamage);
            }
        }

    }
}
