﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    EntityAttributes entity;

    public AudioClip dieSound;
    public GameObject deathParticle;


    private void Awake()
    {
        entity = GetComponent<EntityAttributes>();       

    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.CompareTag("Player"))
        {
            IDamageable entityInterface = (IDamageable)collision.collider.GetComponent(typeof(IDamageable));
            if (entityInterface != null)
            {
                entityInterface.GetDamage(entity.dealDamage);
            }
        }
    }
    public void Die()
    {
        GameObject clone = Instantiate(deathParticle, transform);
        clone.transform.parent = null;
        Destroy(clone, 5f);

        SoundManager.PlaySounds(dieSound);

        Destroy(gameObject);
    }
    
}
