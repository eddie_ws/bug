﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    private Rigidbody rb;

    public float speed = 5f;
    public float jumpSpeed = 5f;

    public Transform from;
    public Transform to;



    private Vector3 moveDirection = Vector3.zero;

    private void Awake()
    {


        rb = GetComponent<Rigidbody>();
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void FixedUpdate()
    {


        moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, 0.0f);
        
        rb.MovePosition(transform.position  + moveDirection * Time.fixedDeltaTime* speed);

        transform.Rotate(0, Input.GetAxis("Horizontal") * 2, 0);

        Debug.Log(moveDirection);


        //transform.rotation.SetEulerAngles(0, 0, 0);

        //if (moveDirection.x > 0)
        //{
        //  transform.Rotate(0f, 0.0f, 0.0f, Space.Self);
        //}

        //if (moveDirection.x < 0)
        //{
        //    transform.Rotate(180.0f, 0.0f, 0.0f, Space.Self);
        //}

        if (Input.GetButtonDown("Jump"))
        {
            Debug.Log("jump");
            rb.velocity = new Vector3(0, jumpSpeed, 0);
        }


    }
}
