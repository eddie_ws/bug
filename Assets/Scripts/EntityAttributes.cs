﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityAttributes : MonoBehaviour, IDamageable
{

    public float health = 1;
    public float dealDamage = 1;

    bool isPlayer = false;
    bool isEnemy = false;

    Vector2 startPosition;

    PlayerController player;
    Enemy enemy;

    private void Awake()
    {
        if (gameObject.CompareTag("Player"))
        {
            isPlayer = true;
            player = GetComponent<PlayerController>();
        }
        if (gameObject.CompareTag("Enemy"))
        {
            isEnemy = true;
            enemy = GetComponent<Enemy>();
        }

        
    }


    void Start()
    {
        startPosition = transform.position;
    }

    public void GetDamage(float damage)
    {
        health -= damage;
        if (health <= 0)
        {
            if (isPlayer)
            {
                
                player.Respawn();

            }
            if (isEnemy)
            {
                enemy.Die();
            }
        }
    }


}
