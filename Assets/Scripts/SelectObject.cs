﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;

public class SelectObject : MonoBehaviour
{
    public TextMeshProUGUI uiTextSelectedObject;

    public GameObject selectedObject;
    string selectedObjectName;
    // Start is called before the first frame update
    void Start()
    {

        uiTextSelectedObject.text = "";
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray screenSelectRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(screenSelectRay.origin, screenSelectRay.direction);
            if ( hit.collider !=  null && !EventSystem.current.IsPointerOverGameObject())
            {
                selectedObject = null;
                uiTextSelectedObject.text = "";
                ObjectReciever.Instance.clearObjectValues();

                

                selectedObject = hit.transform.gameObject;

                if(selectedObject.GetComponent<CanBeSelected>() != null)
                {
                    selectedObject.SendMessage("recieveUIRequest", SendMessageOptions.DontRequireReceiver);
                    uiTextSelectedObject.text = selectedObject.name.ToUpper();
                }
                
                
            }

            else if (EventSystem.current.IsPointerOverGameObject())
            {

            }

            else
            {
                selectedObject = null;
                uiTextSelectedObject.text = "";
                ObjectReciever.Instance.clearObjectValues();
            }
        }   
    }
}
