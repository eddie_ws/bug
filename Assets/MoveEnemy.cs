﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveEnemy : MonoBehaviour
{
    public Vector3 minEnemyPosition;
    public Vector3 maxEnemyPosition;

    public float minSpeed;
    public float maxSpeed;
    public float defaultSpeed;

    public float minScale;
    public float defaultScale;
    public float maxScale;

    Vector3 newPosition;

    float lerper;
    bool moveToMinPos;
    bool moveToMaxPos = true;
    Vector3 direction;

    // Start is called before the first frame update
    void Awake()
    {
        transform.position = minEnemyPosition;

        direction = (maxEnemyPosition - minEnemyPosition).normalized;

        transform.localScale = new Vector3(defaultScale, defaultScale, defaultScale);

        transform.GetChild(1).transform.localScale = new Vector3(-0.54f, 0.54f, 0.54f);
    }

    // Update is called once per frame
    void Update()
    {
        newPosition = Vector3.zero;

        if (moveToMaxPos)
        {
            newPosition = Vector3.Lerp(minEnemyPosition, maxEnemyPosition, lerper);

            lerper += defaultSpeed * Time.deltaTime;

            if (lerper >= 1f)
            {
                transform.GetChild(1).transform.localScale = new Vector3(0.54f, 0.54f, 0.54f);
                moveToMaxPos = false;
                moveToMinPos = true;
            }
        }

        if (moveToMinPos)
        {
            newPosition = Vector3.Lerp(minEnemyPosition, maxEnemyPosition, lerper);

            lerper -= defaultSpeed * Time.deltaTime;

            if (lerper <= 0f)
            {
                transform.GetChild(1).transform.localScale = new Vector3(-0.54f, 0.54f, 0.54f);
                moveToMaxPos = true;
                moveToMinPos = false;
            }
        }

        transform.position = newPosition;

        
    }

    void recieveUIRequest()
    {
        UISlider scale = new UISlider("SCALE", minScale, defaultScale, maxScale);

        UISlider speed = new UISlider("SPEED", minSpeed, defaultSpeed, maxSpeed);

        UISlider[] transforms = new UISlider[2];

        transforms[0] = scale;

        transforms[1] = speed;

        ObjectReciever.Instance.recieveObjectValues(transforms);
    }


    //If the player changes the slider values, we get the updated slider values in a float list. The order of the float list is the same as the UISlider list we created before
    void recieveUpdatedValuesFromUI(float[] newValues)
    {
        defaultScale = newValues[0];
        transform.localScale = new Vector3(defaultScale, defaultScale, defaultScale);

        defaultSpeed = newValues[1];
    }
}
