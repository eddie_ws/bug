﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HeaderCountdown : MonoBehaviour
{
    public string firstLevelScene;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(countDown());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator countDown()
    {
        yield return new WaitForSeconds(5);
        SceneManager.LoadScene(firstLevelScene);
    }
}
